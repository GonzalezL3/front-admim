import React from 'react';

export const Migas =()=>{
  return(
    <>
    <div className="subheader py-2 py-lg-12 subheader-transparent" id>
  <div className="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">    
    <div className="d-flex align-items-center flex-wrap mr-1">      
      <div className="d-flex flex-column">        
        <h2 className="text-white font-weight-bold my-2 mr-5">Base Controls</h2>                
        <div className="d-flex align-items-center font-weight-bold my-2">          
          <button  className="opacity-75 hover-opacity-100">
            <i className="flaticon2-shelter text-white icon-1x" />
          </button>                    
          <span className="label label-dot label-sm bg-white opacity-75 mx-3" />
          <a href="#prueba" className="text-white text-hover-white opacity-75 hover-opacity-100">Crud</a>                    
          <span className="label label-dot label-sm bg-white opacity-75 mx-3" />
          <a href="#prueba" className="text-white text-hover-white opacity-75 hover-opacity-100">Forms &amp; Controls</a>                    
          <span className="label label-dot label-sm bg-white opacity-75 mx-3" />
          <a href="#prueba" className="text-white text-hover-white opacity-75 hover-opacity-100">Form Controls</a>                    
          <span className="label label-dot label-sm bg-white opacity-75 mx-3" />
          <a href="#prueba" className="text-white text-hover-white opacity-75 hover-opacity-100">Base Inputs</a>          
        </div>        
      </div>      
    </div>        
    <div className="d-flex align-items-center">      
      <button  className="btn btn-transparent-white font-weight-bold py-3 px-6 mr-2">Reports</button>         
      <div className="dropdown dropdown-inline ml-2" data-toggle="tooltip" title data-placement="top" data-original-title="Quick actions">
        <button className="btn btn-white font-weight-bold py-3 px-6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
        <div className="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">          
          <ul className="navi navi-hover py-5">
            <li className="navi-item">
              <button className="navi-link">
                <span className="navi-icon">
                  <i className="flaticon2-drop" />
                </span>
                <span className="navi-text">New Group</span>
              </button>
            </li>
            <li className="navi-item">
              <button className="navi-link">
                <span className="navi-icon">
                  <i className="flaticon2-list-3" />
                </span>
                <span className="navi-text">Contacts</span>
              </button>
            </li>
            <li className="navi-item">
              <button className="navi-link">
                <span className="navi-icon">
                  <i className="flaticon2-rocket-1" />
                </span>
                <span className="navi-text">Groups</span>
                <span className="navi-link-badge">
                  <span className="label label-light-primary label-inline font-weight-bold">new</span>
                </span>
              </button>
            </li>
            <li className="navi-item">
              <button className="navi-link">
                <span className="navi-icon">
                  <i className="flaticon2-bell-2" />
                </span>
                <span className="navi-text">Calls</span>
              </button>
            </li>
            <li className="navi-item">
              <button  className="navi-link">
                <span className="navi-icon">
                  <i className="flaticon2-gear" />
                </span>
                <span className="navi-text">Settings</span>
              </button>
            </li>
            <li className="navi-separator my-3" />
            <li className="navi-item">
              <button className="navi-link">
                <span className="navi-icon">
                  <i className="flaticon2-magnifier-tool" />
                </span>
                <span className="navi-text">Help</span>
              </button>
            </li>
            <li className="navi-item">
              <button className="navi-link">
                <span className="navi-icon">
                  <i className="flaticon2-bell-2" />
                </span>
                <span className="navi-text">Privacy</span>
                <span className="navi-link-badge">
                  <span className="label label-light-danger label-rounded font-weight-bold">5</span>
                </span>
              </button>
            </li>
          </ul>          
        </div>
      </div>      
    </div>    
  </div>
</div>
  
    </>
  )
}