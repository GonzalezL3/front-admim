import React from 'react';
import { Link }from 'react-router-dom';

export const Menu = () => {
  return (
    <>
      <div className="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">								
        <div id="kt_header_menu" className="header-menu header-menu-left header-menu-mobile header-menu-layout-default">									
          <ul className="menu-nav">                      
            <li className="menu-item ">
              <Link  to='/cotizacion' className="menu-link  ml-1 mr-1 text-white bg-primary mt-2" >
                cotización              
              </Link>
            </li>
            <li className="menu-item ">
              <Link  to='/servicios' className="menu-link  ml-1 mr-1 text-white bg-primary mt-2" >
                Servicios              
              </Link>
            </li>
            <li className="menu-item ">
              <Link  to='/clientes' className="menu-link  ml-1 mr-1 text-white bg-primary mt-2" >
                Clientes              
              </Link>
            </li>
                
              {/* <Link to='/clientes' className="menu-link menu-toggle">
                <span className="menu-text">CLientes</span>
                <i className="menu-arrow" />
              </Link> */}
            
          </ul>									
        </div>								
      </div>							
    </>
  )
}