import React from 'react'

export const Alerta = ({mensaje}) => {
  return (
    <div className="alert alert-custom alert-primary fade show" role="alert">
      <div className="alert-icon"><i className="flaticon-warning" /></div>
      <div className="alert-text">{mensaje}</div>
      <div className="alert-close">
        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true"><i className="ki ki-close" /></span>
        </button>
      </div>
    </div>

  )
}
