import React from 'react';

export const BodyCliente=()=>{

  
  return(
    <>
      <div className="card card-custom col-5 mt-5">
        <div className="card-header">
          <h3 className="card-title">Registro de clientes</h3>
        </div>          

        <form className="form">
          <div className="card-body">
          <div className="form-group">
            <label>Nombre o razón social del cliente
            <span className="text-danger">*</span></label>
            <input type="text" className="form-control" placeholder="Escribir" />
          </div>
          </div>
          <div className="card-footer">
            <button type="reset" className="btn btn-primary mr-2">Guardar</button>          
          </div>
        </form>          
      </div>  
    </>
  )
}