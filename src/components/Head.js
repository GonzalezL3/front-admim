import React from 'react';
// import { Cliente } from './Cliente';
import { Factura } from './Factura';
import { Foother } from './Foother';
import { Servicios } from './Servicios';
import { Table } from './Table';
import { HeadMovil } from './HeadMovil';
import {HeadG} from './HeadG';

export const Head=()=>{
  return(
    <>     
      <HeadMovil />
      <div className="d-flex flex-column flex-root">	
        <div className="d-flex flex-row flex-column-fluid page">	
          <div className="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">				
            <HeadG/>
            <div className="content d-flex flex-column flex-column-fluid" id="kt_content">	
              <div className="container">
                <div className="row">
                  <Servicios/>
                  <Table titulo='Pruebas'/>
                  <Factura titulo='prueba'/>
                  {/* <Cliente/>
                  <Table titulo='Clientes'/> */}
                </div>				
              </div>				                     					                
            </div>
            <Foother/>
          </div>
        </div>
      </div>      
    </>
  )
}