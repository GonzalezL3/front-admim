
import React,{useState} from 'react';

export const CardPrecios=({titulo,precio,evento})=>{

  const [demo, setdemo] = useState(false)
  return(    
    <div className="card-body animate__animated animate__slideInUp">
      <div className="form-group m-0">
        <div className="alert alert-primary" role="alert">
          <label>{titulo} :</label>
        </div>
      <div className="row">
      <div className="col-lg-6">
        <label className="option">
          <span className="option-control">
            <span className="radio">
              <input type="checkbox" checked={demo} name="m_option_1" value="1" onChange={() => setdemo(!demo)} />
              <span></span>
            </span>
          </span>
          <span className="option-label">
            <span className="option-head">
              <span className="option-title">
                Proyecto
              </span>
              <span className="option-focus">
                Demo
              </span>
            </span>
            <span className="option-body">
              Tiempo estimado de 1 a 30 días
            </span>
          </span>
        </label>
        <div className="col-12 row ">
          <div className="form-group col-12 col-md-6">
            <label>Inicio</label>
            <input type="date" onChange={evento} name="a1" className="form-control" />                  
          </div>

          <div className="form-group col-12 col-md-6">
            <label>Fin</label>
            <input type="date" onChange={evento} name="a2" className="form-control" />                  
          </div>
        </div>
      </div>

      <div className="col-lg-6">
        <label className="option">
          <span className="option-control">
            <span className="radio">
              <input type="checkbox" name="m_option_1" value="1"/>
              <span></span>
            </span>
          </span>
          <span className="option-label">
            <span className="option-head">
              <span className="option-title">
                tiempo de membresía
              </span>
              

              <span className="option-focus">
                $&nbsp;{precio}.00
              </span>
            </span>
            <span className="option-body">
              Tiempo se licencia del producto        
            </span>
          </span>          
        </label>
        <div className="col-12 row ">
          <div className="form-group col-12 col-md-6">
            <label>Inicio</label>
            <input type="date" onChange={evento} name="a3" className="form-control" />                  
          </div>

          <div className="form-group col-12 col-md-6">
            <label>Fin</label>
            <input type="date" onChange={evento} name="a4" className="form-control" />                  
          </div>
        </div>      
      </div>
    </div>
  </div>
</div>
    
  )
}