import React,{useState} from 'react';
import Select  from 'react-select';
import { CardPrecios } from './Card_precios';
import {HeadG} from './HeadG';
import {HeadMovil} from './HeadMovil';
import {Foother} from './Foother';
export const Factura=()=>{
  const [state, setState] = useState([]);
  
  console.log(state,'este es el estado');
  const options = [
    { value: '1', label: 'HTDS' },
    { value: '2', label: 'HAS' },
    { value: '3', label: 'Social media ' }
  ];

  const onChange =({value,label})=>{
    console.log(value);
    console.log(label);
    setState([      
      ...state,
      {'nombre':label,'id':value}      
    ])
  }
  const handleChange=({target})=> {
    let {name,value}=target;
    console.log({name,value},'**************');
  }

  return(
    <>
    <HeadMovil />
      <div className="d-flex flex-column flex-root">	
        <div className="d-flex flex-row flex-column-fluid page">	
          <div className="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">				
            <HeadG/>
            <div className="content d-flex flex-column flex-column-fluid" id="kt_content">	
              <div className="container">
                <div className="row">
                  <div className="card card-custom col-12 mt-5">
                    <div className="card-header">
                      <h3 className="card-title">Servicio o producto</h3>
                    </div>          

                    <form className="form">
                      <div className="card-body">
                        <div className="form-group">
                          <label>Escribir el nombre de producto o servicio</label>
                          <Select options={options}
                                  onChange={onChange}                
                          />
                        </div>                    
                        {
                          state.map(({nombre,id})=>{
                            return  <CardPrecios key={id}
                                      titulo={nombre} 
                                      precio={id} 
                                      evento={handleChange}
                                    />
                          })
                        }
                      </div>
                    </form>
                  </div>
                </div>				
              </div>				                     					                
            </div>
            <Foother/>
          </div>
        </div>
      </div>      
    </>
  )
}