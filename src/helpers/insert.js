export const insert = async (ruta='',json={}) => {
  try {
    const config = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(json)
    }
    const response = await fetch(ruta, config)    
    if (response.ok) {      
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
}