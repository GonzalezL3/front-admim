export const insertCliente = async (cliente) => {
  try {
    const config = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(cliente)
    }
    const response = await fetch('https://humanly-sw.com/api-administrar-sistemas/Cliente', config)
    //const json = await response.json()
    if (response.ok) {
      
      return 1;
    } else {
      return 0;
    }
  } catch (error) {
    return 0;
  }
}