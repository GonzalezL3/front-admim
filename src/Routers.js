import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { Factura } from './components/Factura';
// import { Head } from './components/Head';
import { Servicios } from './components/Servicios';
import { Cliente } from './components/Cliente';



export const Routers=()=> {
  return (
    <Router>
      <Switch>
        <Route exact path='/'>
          <Factura/>
        </Route>

        <Route exact path='/servicios'>
          <Servicios/>
        </Route>

        <Route exact path='/clientes'>
          <Cliente/>
        </Route>

        <Route exact path='/cotizacion'>
          <Factura/>
        </Route>

      </Switch>
    </Router>   
  )
}
